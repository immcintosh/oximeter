# Pulse Oximeter
![Device](pulse_meter.jpeg)

This project includes the hardware design and Firmware code for a working pulse meter.  While the hardware supports full oximeter functionality, the software currently only implements the simpler pulse rate tracking.