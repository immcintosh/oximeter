if ~exist('SOS','var')
	return
end
if ~exist('VarName','var')
	return
end
if ~exist('PathName','var')
	[FileName,PathName]=uiputfile('*.h;*.hpp');
end
[rows, columns]=size(SOS);
fp = fopen([PathName FileName],'wt');
fprintf(fp, '#pragma once\n\n');
fprintf(fp, '#include "../iir.hpp"\n\n');
fprintf(fp, 'static const Math::Q15 %s_coeffs[] {\n', VarName);
for row=1:rows
	fprintf(fp, '\t// Section %d\n', row);
	fprintf(fp, '\t%.13f,\n', SOS(row,1)/2);
	fprintf(fp, '\t%.13f,\n', 0);
	fprintf(fp, '\t%.13f,\n', SOS(row,2)/2);
	fprintf(fp, '\t%.13f,\n', SOS(row,3)/2);
	fprintf(fp, '\t%.13f,\n', -SOS(row,5)/2);
	fprintf(fp, '\t%.13f,\n', -SOS(row,6)/2);
end
fprintf(fp, '};\n');
fprintf(fp, 'Math::IIR<Math::Q15, %d> %s(%s_coeffs, 1);\n', rows, VarName, VarName);
fclose(fp);
