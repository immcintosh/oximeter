#include "main.h"

__attribute__((aligned(256))) dma_control_t dma_channels[16];

void dma_init()
{
	// Enable DMA clock.
	BITBAND(&CMU->HFCORECLKEN0, _CMU_HFCORECLKEN0_DMA_SHIFT) = 1;
	DMA->CTRLBASE = (uint32_t)dma_channels;
}
