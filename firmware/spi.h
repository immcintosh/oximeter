#pragma once

void spi_init(int baud);
void spi_transmit(const void* data, size_t size);
void spi_enable();
void spi_disable();
