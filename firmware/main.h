#pragma once

#include <stdint.h>
#include <stddef.h>
#include <limits.h>
#include <em_device.h>

#include "adc.h"
#include "clock.h"
#include "display.h"
#include "dma.h"
#include "opamp.h"
#include "spi.h"
#include "timer.h"

#include <arm_math.h>
#include <arm_const_structs.h>

#define DISABLE_POWERSAVE 0
#define DRAW_UPSIDE_DOWN 0

#define SAMPLE_PERIOD 66
#define FFT_LEN 128
#define FFT_STRUCT arm_cfft_sR_q15_len128
#define DISPLAY_STRIDE 184
#define DISPLAY_BYTES 23
#define DISPLAY_LINES 38
#define SMALLEST_BIN 6

#define BITBAND(addr, bit) (*(volatile uint32_t*)(0x42000000 + ((((uint32_t)(addr)) - 0x40000000) << 5) + ((bit) << 2)))

void fatal_error();
void fatal_assert(int cond);
