#include "main.h"

void lftimer_init()
{
	CMU->OSCENCMD =
		// Enable LFRC oscillator
		CMU_OSCENCMD_LFRCOEN;
	// Enable LE Peripheral Interface
	BITBAND(&CMU->HFCORECLKEN0, _CMU_HFCORECLKEN0_LE_SHIFT) = 1;
	// Enable LETIMER0 Clock
	BITBAND(&CMU->LFACLKEN0, _CMU_LFACLKEN0_LETIMER0_SHIFT) = 1;
	BITBAND(&CMU->HFPERCLKEN0, _CMU_HFPERCLKEN0_GPIO_SHIFT) = 1;
	GPIO->P[3].MODEL |=
		// Push-Pull D6
		GPIO_P_MODEL_MODE6_PUSHPULL;
	LETIMER0->ROUTE =
		// Route to LOC0, O0=PD6
		LETIMER_ROUTE_LOCATION_LOC0 |
		// Enable O0
		LETIMER_ROUTE_OUT0PEN;
	// Interrupt on underflow
	LETIMER0->IFC = LETIMER_IFC_UF;
	LETIMER0->IEN = LETIMER_IEN_UF;
}

void lftimer_stop()
{
	// Stop the timer.
	LETIMER0->CMD = LETIMER_CMD_STOP;
}

void lftimer_start(int period)
{
	LETIMER0->CTRL =
		// Free run mode
		LETIMER_CTRL_REPMODE_FREE |
		// Underflow pulse
		LETIMER_CTRL_UFOA0_PULSE |
		// Top value is COMP0
		LETIMER_CTRL_COMP0TOP;
	LETIMER0->CNT = period;
	LETIMER0->COMP0 = period;
	LETIMER0->REP0 = 1;
	// Start the timer.
	LETIMER0->CMD = LETIMER_CMD_START;
}

void lftimer_wait()
{
	// If the timer has already expired the timing allotment
	// has been exceeded and the system must be reset.
	if(LETIMER0->IFC & LETIMER_IFS_UF) {
		fatal_error();
	}
	// Wait in DEEPSLEEP until next cycle phase
	SCB->SCR |= SCB_SCR_SLEEPDEEP_Msk;
	// Wait for period timer.
	do {
		__WFE();
	} while(!(LETIMER0->IF & LETIMER_IFS_UF));
	// Clear timer interrupt
	LETIMER0->IFC = LETIMER_IFC_UF;
	NVIC_ClearPendingIRQ(LETIMER0_IRQn);
	// Disable DEEPSLEEP for this phase.
	SCB->SCR &= ~SCB_SCR_SLEEPDEEP_Msk;
}

void timer_init()
{
	// Enable TIMER1 clock
	BITBAND(&CMU->HFPERCLKEN0, _CMU_HFPERCLKEN0_TIMER1_SHIFT) = 1;
	TIMER1->CTRL =
		// Down count
		TIMER_CTRL_MODE_DOWN |
		// One shot
		TIMER_CTRL_OSMEN |
		// Prescale HFPERCLK by 1024
		TIMER_CTRL_PRESC_DIV32;
	// Interrupt on underflow.
	TIMER1->IFC = TIMER_IFC_UF;
	TIMER1->IEN = TIMER_IEN_UF;
	// Disable TIMER1 clock.
	BITBAND(&CMU->HFPERCLKEN0, _CMU_HFPERCLKEN0_TIMER1_SHIFT) = 0;
}

void timer_start(int period)
{
	timer_enable();
	TIMER1->CNT = period;
	TIMER1->CMD = TIMER_CMD_START;
	// Wait for underflow.
	do {
		__WFE();
	} while(!(TIMER1->IF & TIMER_IF_UF));
	// Clear ADC interrupt
	TIMER1->IFC = TIMER_IFC_UF;
	NVIC_ClearPendingIRQ(TIMER1_IRQn);
	timer_disable();
}

void timer_enable()
{
	// Enable TIMER1 clock.
	BITBAND(&CMU->HFPERCLKEN0, _CMU_HFPERCLKEN0_TIMER1_SHIFT) = 1;
}

void timer_disable()
{
	// Disable TIMER1 clock.
	BITBAND(&CMU->HFPERCLKEN0, _CMU_HFPERCLKEN0_TIMER1_SHIFT) = 0;
}
