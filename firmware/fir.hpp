#pragma once

#include "fixed.hpp"

namespace Math
{
template<typename T, size_t NumTaps>
class FIR;

template<size_t NumTaps>
class FIR<Q15, NumTaps>
{
	static_assert(sizeof(Q15[2]) == sizeof(int16_t[2]), "Q type not tightly packed.");
public:
	static constexpr size_t BlockSize = 1;

	FIR(const Q15* coeffs)
	{
		arm_fir_init_q15(&m_instance, NumTaps, reinterpret_cast<q15_t*>(const_cast<Q15*>(coeffs)), reinterpret_cast<q15_t*>(m_state), BlockSize);
	}
	Q15 filter(Q15 input)
	{
		Q15 output;
		arm_fir_q15(&m_instance, reinterpret_cast<q15_t*>(&input), reinterpret_cast<q15_t*>(&output), BlockSize);
		return output;
	}

private:
	arm_fir_instance_q15 m_instance;
	Q15 m_state[NumTaps + BlockSize];
};

template<size_t NumTaps>
class FIR<Q31, NumTaps>
{
	static_assert(sizeof(Q31[2]) == sizeof(int32_t[2]), "Q type not tightly packed.");
public:
	static constexpr size_t BlockSize = 1;

	FIR(const Q31* coeffs)
	{
		arm_fir_init_q31(&m_instance, NumTaps, reinterpret_cast<q31_t*>(const_cast<Q31*>(coeffs)), reinterpret_cast<q31_t*>(m_state), BlockSize);
	}
	Q31 filter(Q31 input)
	{
		Q31 output;
		arm_fir_q31(&m_instance, reinterpret_cast<q31_t*>(&input), reinterpret_cast<q31_t*>(&output), BlockSize);
		return output;
	}

private:
	arm_fir_instance_q31 m_instance;
	Q31 m_state[NumTaps + BlockSize - 1];
};
}
