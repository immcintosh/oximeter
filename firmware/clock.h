#pragma once

extern uint32_t SystemPeripheralClock;

void clock_init(void);
void clock_mode_compute(void);
void clock_mode_low_power(void);
