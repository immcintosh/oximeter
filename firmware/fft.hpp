#pragma once

#include "fixed.hpp"

extern "C" void arm_radix4_butterfly_q15(
	q15_t * pSrc,
	uint32_t fftLen,
	q15_t * pCoef,
	uint32_t twidCoefModifier);

extern "C" void arm_radix4_butterfly_inverse_q15(
	q15_t * pSrc,
	uint32_t fftLen,
	q15_t * pCoef,
	uint32_t twidCoefModifier);

extern "C" void arm_bitreversal_16(
	uint16_t * pSrc,
	const uint16_t bitRevLen,
	const uint16_t * pBitRevTable);

extern "C" void arm_cfft_radix4by2_q15(
	q15_t * pSrc,
	uint32_t fftLen,
	const q15_t * pCoef);

extern "C" void arm_cfft_radix4by2_inverse_q15(
	q15_t * pSrc,
	uint32_t fftLen,
	const q15_t * pCoef);

namespace Math
{
template<typename _T, size_t _Length> class _FFTTypes
{
public:
	typedef _T T;
	static constexpr size_t Length = _Length;

	static T Interpolate(T* bins, size_t centerBin)
	{
		if(centerBin == 0) {
			return bins[0];
		} else if(centerBin == Length - 1) {
			return bins[Length - 1];
		} else {
			typename T::QI b0 = bins[centerBin - 1];
			typename T::QI b1 = bins[centerBin];
			typename T::QI b2 = bins[centerBin + 1];
			typename T::QI num = b2 - b0;
			typename T::QI den = 2 * (2 * b1 - b0 - b2);
			if(den == 0) {
				return 0;
			}
			return num / den;
		}
	}

	static size_t FindHarmonics(const T* bins, size_t* harmonics, size_t fundamental, size_t numHarmonics)
	{
		harmonics[0] = fundamental;
		size_t harmonic = 2;
		size_t index = fundamental * 2;
		while(index < (Length - 1) && harmonic <= numHarmonics) {
			T b0 = bins[index - 1];
			T b1 = bins[index];
			T b2 = bins[index + 1];
			if(b0 > b1) {
				if(b2 > b0) {
					harmonics[harmonic - 1] = index + 1;
				} else {
					harmonics[harmonic - 1] = index - 1;
				}
			} else if(b2 > b1) {
				harmonics[harmonic - 1] = index + 1;
			} else {
				harmonics[harmonic - 1] = index;
			}
			++harmonic;
			index += fundamental;
		}
		return harmonic - 1;
	}
};

template<typename _T, size_t Length> class _FFT;
template<> class _FFT<Q15, 128> : public _FFTTypes<Q15, 128>
{
protected:
	static const arm_cfft_instance_q15* Instance() { return &arm_cfft_sR_q15_len128; }
};

template<typename _T, size_t _Length>
class FFT;

template<> class FFT<Q15, 128> : public _FFT<Q15, 128>
{
public:
	static void Transform(T* samples)
	{
		arm_cfft_radix4by2_q15(
			reinterpret_cast<q15_t*>(samples),
			128,
			(q15_t*)arm_cfft_sR_q15_len128.pTwiddle
			);
		arm_bitreversal_16((uint16_t*)reinterpret_cast<q15_t*>(samples), arm_cfft_sR_q15_len128.bitRevLength, arm_cfft_sR_q15_len128.pBitRevTable);
	}
	static void Magnitude(T* samples)
	{
		arm_cmplx_mag_q15(reinterpret_cast<q15_t*>(samples), reinterpret_cast<q15_t*>(samples), FFT_LEN);
	}
};
}
