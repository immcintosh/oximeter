#include "main.h"

uint32_t SystemCoreClock = 20000000;
uint32_t SystemPeripheralClock = 14000000;

void clock_init(void)
{
	CMU->OSCENCMD =
		// Disable unused clocks.
		CMU_OSCENCMD_HFXODIS | CMU_OSCENCMD_AUXHFRCODIS | CMU_OSCENCMD_LFRCODIS |
		// Enable HF RC clock.
		CMU_OSCENCMD_HFRCOEN |
		// Enable LF XO clock.
		CMU_OSCENCMD_LFXOEN;
	clock_mode_low_power();
	// Set default clock speeds.
	SystemCoreClock = 7000000 >> 2;
	SystemPeripheralClock = 7000000 >> 2;
	// Select LF RC oscillator as LFA clock source
	CMU->LFCLKSEL = CMU_LFCLKSEL_LFA_LFXO;
	// Set LF RC prescaler
	CMU->LFAPRESC0 = 5 << 8;
}

void clock_mode_compute(void)
{
	// Set to one wait state with SCBTP.
	MSC->READCTRL = MSC_READCTRL_MODE_WS1SCBTP;
	// Set HF RC tuning for absolute maximum speed.
	CMU->HFRCOCTRL = (255 & 0xFF) | CMU_HFRCOCTRL_BAND_28MHZ;
	// No clock divider.
	CMU->HFCORECLKDIV = 0;
	// Disable peripheral clock.
	CMU->HFPERCLKDIV = 0;
}

void clock_mode_low_power(void)
{
	// Set HF RC tuning to get a good SPI rate in 7MHz band.
	CMU->HFRCOCTRL = (200 & 0xFF) | CMU_HFRCOCTRL_BAND_7MHZ;
	// Set core clock divider.
	CMU->HFCORECLKDIV = 2;
	// Set peripheral clock divider.
	CMU->HFPERCLKDIV = 2 | (1 << 8);
	// Set to zero wait state with SCBTP.
	MSC->READCTRL = MSC_READCTRL_MODE_WS0SCBTP;
}
