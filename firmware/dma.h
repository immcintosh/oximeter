#pragma once

typedef struct __attribute__((packed))
{
	uint32_t Source;
	uint32_t Destination;
	uint32_t CycleCtrl : 3;
	uint32_t NextUseBurst : 1;
	uint32_t NMinus1 : 10;
	uint32_t RPower : 4;
	uint32_t SrcProtCtrl : 3;
	uint32_t DstProtCtrl : 3;
	uint32_t SrcSize : 2;
	uint32_t SrcInc : 2;
	uint32_t DstSize : 2;
	uint32_t DstInc : 2;
	uint32_t User;
} dma_control_t;

extern dma_control_t dma_channels[16];

void dma_init();
