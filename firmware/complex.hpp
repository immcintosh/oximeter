#pragma once

namespace Math
{
template<typename T> class Complex
{
public:
	Complex(T real)
		: m_r(real)
	{
	}
	Complex(T real, T complex)
		: m_r(real)
		, m_c(complex)
	{
	}
	T R() const { return m_r; }
	T C() const { return m_c; }

private:
	T m_r;
	T m_c;
};
}
