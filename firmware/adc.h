#pragma once

#define ADC_INPUT_TIA ADC_SINGLECTRL_INPUTSEL_CH1
#define ADC_INPUT_AMP ADC_SINGLECTRL_INPUTSEL_CH5
#define ADC_INPUT ADC_INPUT_TIA

void adc_init();
void adc_enable();
void adc_disable();
void adc_sample();
uint16_t adc_get_sample();
