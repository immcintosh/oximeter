/**
 * 			|------------------- 100 ms -------------------|
 * SAMPLE	 ##--------------------------------------------
 * CALC		 --##------------------------------------------
 * RENDER	 ----#####-------------------------------------
 * WAIT		 ---------#####################################
 */

extern "C" {
#include "main.h"
}

#include "fixed.hpp"
#include "fir.hpp"
#include "iir.hpp"
#include "coefficients.h"
#include "ring.hpp"
#include "fft.hpp"
#include "filters/display.h"
#include "filters/band_1_2.h"

typedef RingIndex<FFT_LEN> FFTIndex;

using Math::Q15;
using Math::Q31;
typedef Math::FFT<Q15, FFT_LEN> FFT;

struct
{
	Q15 current_result;
	Q15 current_filtered;
	Q15 bins[FFT_LEN * 2];
	Ring<Q15, FFT_LEN> input;
	int16_t gain_offset;
	int largest_bin;
	int last_largest_bin;
	bool edge_ready;
	Q15::QI range;
	Q15::QI input_scale;
} _data;

void fatal_error(void)
{
	__asm volatile("bkpt 0");
}

void fatal_assert(int cond)
{
	if(!cond) {
		fatal_error();
	}
}

/**
 * @brief Sample phase
 */
void phase_sample(void)
{
	// Enable peripherals
	adc_enable();
	opamp_enable(_data.gain_offset);
	timer_start(1);
	// Initiate ADC sampling and wait for it to complete
	adc_sample();
	// Acquire result.  Adjust input sample to be zero-centered and convert to
	// fixed point format.
	_data.current_result = Q15::make(Q15::TI(adc_get_sample()) - Q15::MinValue);
	// Disable peripherals
	opamp_disable();
	adc_disable();
}

/**
 * @brief Calculate phase
 */
void phase_calculate(void)
{
	// Enter compute mode.
	clock_mode_compute();
	// Perform calculations
	{
		Q15 value = _filt_display.filter(_data.current_result);

		// Store value for FFT calculation.
		_data.input.add(value);

		// Do some scaling.
		static int count = 0;
		if(++count == 20) {
			Q15::QI min = Q15::QI::make(Q15::QI::MaxValue);
			Q15::QI max = Q15::QI::make(Q15::QI::MinValue);
			for(auto i = _data.input.index() - 15; i != _data.input.index(); ++i) {
				Q15::QI value = _data.input[i];
				if(value < min) {
					min = value;
				}
				if(value > max) {
					max = value;
				}
			}
			_data.range = max - min;
			Q15::QI newScale = Q15::QI(0.75) / _data.range;
			display_draw_zoom(_data.input_scale.get());
			if(newScale * 8 < _data.input_scale) {
				for(auto i = _data.input.index() + 1; i != _data.input.index(); ++i) {
					_data.input[i] = value;
				}
			}
			_data.input_scale = newScale;
			count = 0;
		}
		value = value * _data.input_scale;

		// Transform value to screen coordinates.
		Q15 screenValue = value * Q15::make(19);
		screenValue += Q15::make(19);
	#if DRAW_UPSIDE_DOWN
		screenValue = Q15::make(38) - screenValue;
	#endif

		// Calculate edge detection.
		Q15 bandValue = _filt_band_1_2.filter(_data.current_result);

		// Draw with or without edge line.
		if(bandValue >= 0 && _data.edge_ready) {
			display_shift_lines_edge(screenValue.get());
			_data.edge_ready = false;
		} else {
			display_shift_lines(screenValue.get());
		}
		if(bandValue < Q15::make(-10)) {
			_data.edge_ready = true;
		}

		// Calculate FFT if enough samples have been acquired.
		if(1) {
			auto index = _data.input.index();
			for(int i = 0; i < FFT_LEN; ++i, ++index) {
				_data.bins[i * 2] = _data.input[index] * _data.input_scale * _gausswin[i];
				_data.bins[i * 2 + 1] = 0;

			}
			FFT::Transform(_data.bins);
			FFT::Magnitude(_data.bins);
			// Find the largest bin.
			_data.last_largest_bin = _data.largest_bin;
			_data.largest_bin = 6;
			for(int i = SMALLEST_BIN; i < (FFT_LEN / 2) - 1; ++i) {
				if(_data.bins[i] > _data.bins[_data.largest_bin]) {
					_data.largest_bin = i;
				}
			}
		#if 1
			Q15 fftRatio = Q15::make(30 * int32_t(Q15::MaxValue) / int32_t(_data.bins[_data.largest_bin].get()));
			// Draw FFT spectrum.
			for(int i = 0; i < 53; ++i) {
				Q15::QI pixel = _data.bins[i] * fftRatio;
				// This is necessary because the largest_bin ignores some of the lower frequencies.
				if(pixel.get() > 30) {
					pixel = Q15::make(30);
				}
				for(int line = 0; line < pixel.get(); ++line) {
					display_set_pixel(line, 78 + i, 1);
				}
				for(int line = pixel.get(); line < 30; ++line) {
					display_set_pixel(line, 78 + i, 0);
				}
			}
			display_draw_notch(78 + _data.last_largest_bin, 78 + _data.largest_bin);
		#endif
			// Perform FFT bin interpolation.
			Math::Q<int32_t, 15> bin = FFT::Interpolate(_data.bins, _data.largest_bin);
			Math::Q<int32_t, 15> largest = _data.largest_bin;
			Math::Q<int32_t, 15> ratio = 7.102272;
			Math::Q<int32_t, 15> bpm1 = (largest) * ratio;
			Math::Q<int32_t, 15> bpm = (largest + bin) * ratio;
			// Draw BPM number.
			display_draw_bpm(bpm.whole());
		}
	}
	// Restore low power mode.
	clock_mode_low_power();
}

/**
 * @brief Render phase
 */
void phase_render(void)
{
	// Enable peripherals
	spi_enable();
	// Toggle the COM pin
	GPIO->P[5].DOUTTGL = 1 << 4;
	// Render the framebuffer
	display_render();
	// Disable peripherals
	spi_disable();
}

/**
 * @brief Wait phase
 */
void phase_wait()
{
	// Wait for the next LETIMER tick.
	lftimer_wait();
}

int main(void)
{
	// Initialize system.
	clock_init();

	// Initialize peripherals.
	dma_init();
	spi_init(1000000);
	lftimer_init();
	timer_init();
	opamp_init();
	adc_init();
	GPIO->P[5].MODEL |=
		// Push-Pull F4
		GPIO_P_MODEL_MODE4_PUSHPULL |
		// Push-Pull F5
		GPIO_P_MODEL_MODE5_PUSHPULL;
	GPIO->P[5].DOUTSET = 1 << 5;

	display_init();
	for(int i = 0; i < FFT_LEN; ++i) {
		_data.bins[i] = 0;
	}
	_data.largest_bin = 0;
	_data.last_largest_bin = 0;
	_data.current_result = 0;
	_data.edge_ready = false;
	_data.input_scale = 1;

	// Sleep on exit and SEV on pending interrupt.
	SCB->SCR |= SCB_SCR_SLEEPONEXIT_Msk | SCB_SCR_SEVONPEND_Msk;
	// Clear any pending events.
	__SEV();
	__WFE();
	// Start the period timer.
	lftimer_start(SAMPLE_PERIOD);

	// Master control loop.
	while(1) {
		phase_sample();
		phase_calculate();
		phase_render();
		phase_wait();
	}
}
