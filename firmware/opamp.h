#pragma once

void opamp_init();
void opamp_enable(uint16_t offset);
void opamp_disable();
