#pragma once

#define ALWAYS_INLINE __attribute__((always_inline))
#define ALWAYS_OPTIMIZE __attribute__((optimize("O3")))
