#include "main.h"

#include "fonts.h"

typedef struct __attribute__((packed))
{
	uint8_t is_update : 1;
	uint8_t com : 1;
	uint8_t is_clear : 1;
	uint8_t pad_1 : 5;
	uint8_t address;
	uint8_t data[DISPLAY_BYTES];
	uint8_t pad_2;
} display_line_t;

typedef struct __attribute__((packed))
{
	display_line_t lines[DISPLAY_LINES];
	uint8_t pad;
} display_framebuffer_t;

static display_framebuffer_t _framebuffer;
static int _last_active_line = 0;

static int _is_in_range(int line, int pixel)
{
	return (line >= 0 && line < DISPLAY_LINES) && (pixel >= 0 && pixel < DISPLAY_STRIDE);
}

static int _clamp_to_range(int line)
{
	return line < 0 ? 0 : line >= DISPLAY_LINES ? DISPLAY_LINES - 1 : line;
}

void display_shift_pixel(void* line, int value);

void display_init()
{
	// Initialize data
	_framebuffer.lines[0].is_update = 1;
	_framebuffer.lines[0].is_clear = 0;
	for(int i = 0; i < DISPLAY_LINES; ++i) {
		// Format address for MSB first transmission.
		_framebuffer.lines[i].address = i + 1;
		// Draw border
		display_set_pixel(i, 65, i & 1);
		display_set_pixel(i, 64, !(i & 1));
		for(int j = 0; j < 8; ++j) {
			_framebuffer.lines[i].data[j] = 0xFF;
		}
	}
	display_draw_zoom(0);
}

void display_draw_zoom(int scale)
{
	int level = 0;
	if(scale >= 0) {
		scale >>= 15;
		while(scale) {
			++level;
			scale >>= 1;
		}
	}
	for(int i = 0; i <= 8; ++i) {
		int val1 = i == level;
		int val2 = i <= level;
		int y = 20 + i * 2;
		display_set_pixel(y, 67, val1);
		display_set_pixel(y, 67, val1);
		display_set_pixel(y, 70, val1);
		display_set_pixel(y, 70, val1);
		display_set_pixel(y, 68, val2);
		display_set_pixel(y, 68, val2);
		display_set_pixel(y, 69, val2);
		display_set_pixel(y, 69, val2);
		display_set_pixel(y + 1, 67, val1);
		display_set_pixel(y + 1, 67, val1);
		display_set_pixel(y + 1, 70, val1);
		display_set_pixel(y + 1, 70, val1);
		display_set_pixel(y + 1, 68, val1);
		display_set_pixel(y + 1, 68, val1);
		display_set_pixel(y + 1, 69, val1);
		display_set_pixel(y + 1, 69, val1);
	}
	for(int i = 0; i <= 8; ++i) {
		int val1 = i == -level;
		int val2 = i <= -level;
		int y = 17 - i * 2;
		display_set_pixel(y, 67, val1);
		display_set_pixel(y, 67, val1);
		display_set_pixel(y, 70, val1);
		display_set_pixel(y, 70, val1);
		display_set_pixel(y, 68, val2);
		display_set_pixel(y, 68, val2);
		display_set_pixel(y, 69, val2);
		display_set_pixel(y, 69, val2);
		display_set_pixel(y - 1, 67, val1);
		display_set_pixel(y - 1, 67, val1);
		display_set_pixel(y - 1, 70, val1);
		display_set_pixel(y - 1, 70, val1);
		display_set_pixel(y - 1, 68, val1);
		display_set_pixel(y - 1, 68, val1);
		display_set_pixel(y - 1, 69, val1);
		display_set_pixel(y - 1, 69, val1);
	}
}

void display_draw_text(int x, int y, const char* text)
{
	do {
		const uint8_t* ch = &font8x8_basic[(*text) * 8];
		_framebuffer.lines[y].data[x] = __RBIT(ch[0]) >> 24;
		_framebuffer.lines[y - 1].data[x] = __RBIT(ch[1]) >> 24;
		_framebuffer.lines[y - 2].data[x] = __RBIT(ch[2]) >> 24;
		_framebuffer.lines[y - 3].data[x] = __RBIT(ch[3]) >> 24;
		_framebuffer.lines[y - 4].data[x] = __RBIT(ch[4]) >> 24;
		_framebuffer.lines[y - 5].data[x] = __RBIT(ch[5]) >> 24;
		_framebuffer.lines[y - 6].data[x] = __RBIT(ch[6]) >> 24;
		_framebuffer.lines[y - 7].data[x] = __RBIT(ch[7]) >> 24;
		--x;
		++text;
	} while(*text != '\0');
}

void display_set_pixel(int line, int pixel, int value)
{
	if(!_is_in_range(line, pixel)) {
		return;
	}
	int word = pixel >> 5;
	int bit = pixel & 31;
	uint32_t* data = ((uint32_t*)_framebuffer.lines[line].data) + word;
	*data = (*data & ~(1 << bit)) | (value << bit);
}

void display_shift_lines(int activeLine)
{
	activeLine = _clamp_to_range(activeLine);
	q15_t minValue;
	q15_t maxValue;
	if(activeLine > _last_active_line) {
		minValue = _last_active_line;
		maxValue = activeLine;
	} else if(activeLine < _last_active_line) {
		minValue = activeLine;
		maxValue = _last_active_line;
	} else {
		minValue = activeLine - 1;
		maxValue = activeLine;
	}
	for(int line = 0; line <= minValue; ++line) {
		display_shift_pixel(_framebuffer.lines[line].data, 1);
	}
	for(int line = minValue + 1; line <= maxValue; ++line) {
		display_shift_pixel(_framebuffer.lines[line].data, 0);
	}
	for(int line = maxValue + 1; line < DISPLAY_LINES; ++line) {
		display_shift_pixel(_framebuffer.lines[line].data, 1);
	}
	_last_active_line = activeLine;
}

void display_shift_lines_edge(int activeLine)
{
	activeLine = _clamp_to_range(activeLine);
	q15_t minValue;
	q15_t maxValue;
	if(activeLine > _last_active_line) {
		minValue = _last_active_line;
		maxValue = activeLine;
	} else if(activeLine < _last_active_line) {
		minValue = activeLine;
		maxValue = _last_active_line;
	} else {
		minValue = activeLine - 1;
		maxValue = activeLine;
	}
	for(int line = 0; line <= minValue; ++line) {
		display_shift_pixel(_framebuffer.lines[line].data, line & 1);
	}
	for(int line = minValue + 1; line <= maxValue; ++line) {
		display_shift_pixel(_framebuffer.lines[line].data, 0);
	}
	for(int line = maxValue + 1; line < DISPLAY_LINES; ++line) {
		display_shift_pixel(_framebuffer.lines[line].data, line & 1);
	}
	_last_active_line = activeLine;
}

void display_draw_bpm(int value)
{
	if(value < 0) {
		value = 0;
	} else if(value > 240) {
		value = 240;
	}
	if(value >= 100) {
		const uint8_t* data[] = {
			&font_impact[value % 10 * 38],
			&font_impact[value / 10 % 10 * 38],
			&font_impact[value / 100 % 10 * 38],
		};
		for(int y = 0; y < 19; ++y) {
			uint8_t* line = _framebuffer.lines[32 - y].data;
			line[17] = __RBIT(data[0][1]) >> 24;
			line[18] = __RBIT(data[0][0]) >> 24;
			line[19] = __RBIT(data[1][1]) >> 28 | __RBIT(data[1][0]) >> 20;
			line[20] = __RBIT(data[1][0]) >> 28 | __RBIT(data[2][1]) >> 20;
			line[21] = __RBIT(data[2][0]) >> 24;
			data[0] += 2;
			data[1] += 2;
			data[2] += 2;
		}
	} else {
		const uint8_t* data[] = {
			&font_impact[value % 10 * 38],
			&font_impact[value / 10 % 10 * 38],
		};
		for(int y = 0; y < 19; ++y) {
			uint8_t* line = _framebuffer.lines[32 - y].data;
			line[17] = __RBIT(data[0][1]) >> 24;
			line[18] = __RBIT(data[0][0]) >> 24;
			line[19] = __RBIT(data[1][1]) >> 28 | __RBIT(data[1][0]) >> 20;
			line[20] = __RBIT(data[1][0]) >> 28;
			line[21] = 0;
			data[0] += 2;
			data[1] += 2;
		}
	}
	for(int y = 0; y < 8; ++y) {
		for(int x = 0; x < 3; ++x) {
			_framebuffer.lines[10 - y].data[20 - x] = __RBIT(font_bpm_off[y * 3 + x]) >> 24;
		}
	}
}

void display_draw_notch(int oldPixel, int newPixel)
{
	if(oldPixel < SMALLEST_BIN) {
		oldPixel = SMALLEST_BIN;
	}
	if(newPixel < SMALLEST_BIN) {
		newPixel = SMALLEST_BIN;
	}
	for(int i = -2; i <= 2; ++i) {
		display_set_pixel(DISPLAY_LINES - 2, oldPixel + i, 0);
		display_set_pixel(DISPLAY_LINES - 2, newPixel + i, 1);
	}
	for(int i = -1; i <= 1; ++i) {
		display_set_pixel(DISPLAY_LINES - 3, oldPixel + i, 0);
		display_set_pixel(DISPLAY_LINES - 3, newPixel + i, 1);
	}
	display_set_pixel(DISPLAY_LINES - 4, oldPixel, 0);
	display_set_pixel(DISPLAY_LINES - 4, newPixel, 1);
}

void display_render()
{
	// Start SPI transmission and wait for it to finish
	spi_transmit(&_framebuffer, sizeof(_framebuffer));
}
