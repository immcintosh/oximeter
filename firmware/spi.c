#include "main.h"

void spi_init(int baud)
{
	// CLOCK
	CMU->HFPERCLKEN0 |=
		// Enable USART0 clock
		CMU_HFPERCLKEN0_USART0 |
		// Enable GPIO clock
		CMU_HFPERCLKEN0_GPIO;

	// DMA
	dma_channels[0].Source = 0;
	dma_channels[0].SrcSize = 0;
	dma_channels[0].SrcInc = 0;
	dma_channels[0].Destination = (uint32_t)&USART0->TXDATA;
	dma_channels[0].DstInc = 0b11;
	dma_channels[0].DstSize = 0;
	dma_channels[0].NextUseBurst = 0;
	dma_channels[0].RPower = 1;
	dma_channels[0].SrcProtCtrl = 0;
	dma_channels[0].DstProtCtrl = 0;
	dma_channels[8].Source = 0;
	dma_channels[8].SrcSize = 0;
	dma_channels[8].SrcInc = 0;
	dma_channels[8].Destination = (uint32_t)&USART0->TXDATA;
	dma_channels[8].DstInc = 0b11;
	dma_channels[8].DstSize = 0;
	dma_channels[8].NextUseBurst = 0;
	dma_channels[8].RPower = 1;
	dma_channels[8].SrcProtCtrl = 0;
	dma_channels[8].DstProtCtrl = 0;
	DMA->CH[0].CTRL =
		// TXBL signal
		DMA_CH_CTRL_SIGSEL_USART0TXBL |
		// USART0
		DMA_CH_CTRL_SOURCESEL_USART0;
	DMA->CONFIG =
		// Enable DMA
		DMA_CONFIG_EN;
	DMA->CHUSEBURSTC =
		// Clear burst mode
		DMA_CHUSEBURSTC_CH0USEBURSTC;
	DMA->CHREQMASKC =
		// Clear channel request mask
		DMA_CHREQMASKC_CH0REQMASKC;

	// UART
	USART0->ROUTE =
		// Enable TX pin
		USART_ROUTE_TXPEN |
		// Enable CS pin
		USART_ROUTE_CSPEN |
		// Enable CLK pin
		USART_ROUTE_CLKPEN |
		// Route to LOC0
		USART_ROUTE_LOCATION_LOC0;
	USART0->CTRL =
		// Synchronous mode
		USART_CTRL_SYNC |
		// TX buffer half full
		USART_CTRL_TXBIL_HALFFULL |
		// CS active high
		USART_CTRL_CSINV |
		// Automatic chip select
		USART_CTRL_AUTOCS |
		// Automatic TX tristate
		USART_CTRL_AUTOTRI;
	if(baud * 2 >= SystemPeripheralClock) {
		USART0->CLKDIV = 0;
	} else {
		USART0->CLKDIV = (256 * (SystemPeripheralClock / (2 * baud) - 1));
	}
	USART0->CMD =
		// Disable receiver
		USART_CMD_RXDIS |
		// Disable transmitter
		USART_CMD_TXDIS |
		// Set master mode
		USART_CMD_MASTEREN;
	GPIO->P[4].MODEH |=
		// Push-Pull E10
		GPIO_P_MODEH_MODE10_PUSHPULL |
		// Push-Pull E12
		GPIO_P_MODEH_MODE12_PUSHPULL |
		// Push-Pull E13
		GPIO_P_MODEH_MODE13_PUSHPULL;
	USART0->IFC = USART_IFC_TXC;
	USART0->IEN = USART_IEN_TXC;
}

void spi_transmit(const void* data, size_t size)
{
	USART0->CMD =
		// Disable receiver
		USART_CMD_RXDIS |
		// Enable transmitter
		USART_CMD_TXEN |
		// Disable transmitter tristate
		USART_CMD_TXTRIDIS |
		// Clear TX buffers
		USART_CMD_CLEARTX;
	if(size > 1024) {
		size_t extraSize = size - 1024 - 1;
		size = 1024;
		dma_channels[8].Source = ((uint32_t)data) + 1024 + extraSize;
		dma_channels[8].NMinus1 = extraSize;
		dma_channels[8].CycleCtrl = 0b001;
		dma_channels[0].CycleCtrl = 0b011;
	} else {
		dma_channels[0].CycleCtrl = 0b001;
	}
	size -= 1;
	dma_channels[0].Source = ((uint32_t)data) + size;
	dma_channels[0].NMinus1 = size;
	DMA->ERRORC = DMA_ERRORC_ERRORC;
	DMA->CHALTC = DMA_CHALTC_CH0ALTC;
	DMA->CHENS = DMA_CHENS_CH0ENS;
	// Wait for transmit complete.
	do {
		__WFE();
	} while(!(USART0->IF & USART_IFS_TXC));
	// Clear SPI interrupt
	USART0->IFC = USART_IFC_TXC;
	NVIC_ClearPendingIRQ(USART0_TX_IRQn);
}

void spi_enable()
{
	// Enable USART0 clock.
	BITBAND(&CMU->HFPERCLKEN0, _CMU_HFPERCLKEN0_USART0_SHIFT) = 1;
}

void spi_disable()
{
	// Disable USART0 clock.
	BITBAND(&CMU->HFPERCLKEN0, _CMU_HFPERCLKEN0_USART0_SHIFT) = 0;
}
