#pragma once

#include <stddef.h>
#include <arm_math.h>
#include "fixed.hpp"

namespace Math
{
template<typename T, size_t NumStages>
class IIR;

template<size_t NumStages>
class IIR<Q15, NumStages>
{
public:
	IIR(const Q15* coeffs, size_t shift)
	{
		memset(m_state, 0, sizeof(m_state));
		arm_biquad_cascade_df1_init_q15(&m_instance, NumStages, const_cast<q15_t*>(reinterpret_cast<const q15_t*>(coeffs)), reinterpret_cast<q15_t*>(m_state), shift);
	}
	Q15 filter(Q15 value)
	{
		Q15 result;
		arm_biquad_cascade_df1_q15(&m_instance, reinterpret_cast<q15_t*>(&value), reinterpret_cast<q15_t*>(&result), 1);
		return result;
	}

private:
	arm_biquad_casd_df1_inst_q15 m_instance;
	Q15 m_state[NumStages * 4];
};

template<size_t NumStages>
class IIR<Q31, NumStages>
{
public:
	IIR(const Q31* coeffs, size_t shift)
	{
		memset(m_state, 0, sizeof(m_state));
		arm_biquad_cas_df1_32x64_init_q31(&m_instance, NumStages, const_cast<q31_t*>(reinterpret_cast<const q31_t*>(coeffs)), m_state, shift);
	}
	Q31 filter(Q31 value)
	{
		Q31 result;
		arm_biquad_cas_df1_32x64_q31(&m_instance, reinterpret_cast<q31_t*>(&value), reinterpret_cast<q31_t*>(&result), 1);
		return result;
	}

private:
	arm_biquad_cas_df1_32x64_ins_q31 m_instance;
	int64_t m_state[NumStages * 4];
};

template<size_t NumStages>
class IIR<float, NumStages>
{
public:
	IIR(const float* coeffs)
	{
		memset(m_state, 0, sizeof(m_state));
		arm_biquad_cascade_df1_init_f32(&m_instance, NumStages, const_cast<float*>(coeffs), m_state);
	}
	float filter(float value)
	{
		float result;
		arm_biquad_cascade_df1_f32(&m_instance, &value, &result, 1);
		return result;
	}

private:
	arm_biquad_casd_df1_inst_f32 m_instance;
	float m_state[NumStages * 4];
};
}
