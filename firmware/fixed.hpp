#pragma once

#include <stdint.h>
#include <stddef.h>
#include "complex.hpp"

namespace Math
{
template<typename T> struct Intermediate;
template<> struct Intermediate<int16_t> { typedef int32_t Type; };
template<> struct Intermediate<int32_t> { typedef int64_t Type; };
template<> struct Intermediate<float> { typedef float Type; };
template<> struct Intermediate<double> { typedef double Type; };

template<typename _T, int _N>
class Q
{
	static_assert(sizeof(_T) * 8 > _N, "Storage class insufficient for bits");
public:
	typedef _T T;
	typedef typename Intermediate<T>::Type TI;
	typedef Q<TI, _N> QI;

	static constexpr int N = _N;
	static constexpr T MaxValue = (TI(1) << ((sizeof(T) * 8) - 1)) - 1;
	static constexpr T MinValue = -(TI(1) << ((sizeof(T) * 8) - 1));
	static constexpr float FloatFactor = float(TI(1) << N);
	static constexpr double DoubleFactor = double(TI(1) << N);

	Q<_T, _N>() : m_value(0) {}
	template<typename U> Q<_T, _N>(U value)
		: m_value(value << N)
	{
	}
	constexpr Q<_T, _N>(float value)
		: m_value(Saturate(value * FloatFactor + ((value > 0) ? 0.5f : -0.5f)))
	{
	}
	constexpr Q<_T, _N>(double value)
		: m_value(Saturate(value * DoubleFactor + ((value > 0) ? 0.5 : -0.5)))
	{
	}
	template<typename U, int M> constexpr Q<_T, _N>(const Q<U, M>& other)
		: m_value((N > M) ? static_cast<T>(other.get()) << (N - M) : static_cast<T>(other.get() >> (M - N)))
	{
	}
	static Q<_T, _N> make(T value) { Q<_T, _N> result; result.m_value = value; return result; }
	explicit operator T() const { return m_value; }
	bool operator >(Q<_T, _N> other) const { return m_value > other.m_value; }
	bool operator <(Q<_T, _N> other) const { return m_value < other.m_value; }
	bool operator >=(Q<_T, _N> other) const { return m_value >= other.m_value; }
	bool operator <=(Q<_T, _N> other) const { return m_value <= other.m_value; }
	bool operator ==(Q<_T, _N> other) const { return m_value == other.m_value; }
	bool operator !=(Q<_T, _N> other) const { return m_value != other.m_value; }
	Q<_T, _N> operator<<(int shift) const { return make(m_value << shift); }
	Q<_T, _N> operator>>(int shift) const { return make(m_value >> shift); }
	Q<_T, _N>& operator<<=(int shift) { m_value <<= shift; return *this; }
	Q<_T, _N>& operator>>=(int shift) { m_value >>= shift; return *this; }
	Q<_T, _N> operator +(Q<_T, _N> other) const { return make(Saturate(TI(m_value) + TI(other.m_value))); }
	Q<_T, _N> operator -(Q<_T, _N> other) const { return make(Saturate(TI(m_value) - TI(other.m_value))); }
	Q<_T, _N> operator *(Q<_T, _N> other) const
	{
		TI tmp = TI(m_value) * TI(other.m_value);
		tmp += 1 << (N - 1);
		return make(tmp >> N);
	}
	Q<_T, _N> operator /(Q<_T, _N> other) const
	{
		TI tmp = TI(m_value) << N;
		if((tmp >= 0 && other.m_value >= 0) || (tmp <= 0 && other.m_value <= 0)) {
			tmp += other.m_value / 2;
		} else {
			tmp -= other.m_value / 2;
		}
		return make(tmp / other.m_value);
	}
	Q<_T, _N>& operator+=(Q<_T, _N> other) { *this = operator+(other); return *this; }
	Q<_T, _N>& operator-=(Q<_T, _N> other) { *this = operator-(other); return *this; }
	Q<_T, _N>& operator*=(Q<_T, _N> other) { *this = operator*(other); return *this; }
	Q<_T, _N>& operator/=(Q<_T, _N> other) { *this = operator/(other); return *this; }
	T get() const { return m_value; }
	T whole() const { return m_value >> N; }

private:
	T m_value;

	template<typename U>
	static constexpr T Saturate(U value)
	{
		return (value > MaxValue) ? MaxValue :
			(value < MinValue) ? MinValue : value;
	}
};

template<typename T, int N, typename U> Q<T, N> operator+(U value, Q<T, N> q)
{
	return Q<T, N>(value) + q;
}

template<typename T, int N, typename U> Q<T, N> operator-(U value, Q<T, N> q)
{
	return Q<T, N>(value) - q;
}

template<typename T, int N, typename U> Q<T, N> operator*(U value, Q<T, N> q)
{
	return Q<T, N>(value) * q;
}

template<typename T, int N, typename U> Q<T, N> operator/(U value, Q<T, N> q)
{
	return Q<T, N>(value) / q;
}

typedef Q<int16_t, 15> Q15;
typedef Complex<Q15> ComplexQ15;
typedef Q<int32_t, 31> Q31;
typedef Complex<Q31> ComplexQ31;
}
