#include "main.h"

void opamp_init()
{
	// Enable DAC0 clock
	BITBAND(&CMU->HFPERCLKEN0, _CMU_HFPERCLKEN0_DAC0_SHIFT) = 1;
	// Disable DAC channels for configuration.
	BITBAND(&DAC0->CH1CTRL, _DAC_CH1CTRL_EN_SHIFT) = 0;
#if ADC_INPUT == ADC_INPUT_AMP
	BITBAND(&DAC0->CH0CTRL, _DAC_CH0CTRL_EN_SHIFT) = 0;
	DAC0->CTRL =
		// Continuous conversion.
		DAC_CTRL_CONVMODE_CONTINUOUS |
		// Output to pin and ADC.
		DAC_CTRL_OUTMODE_PINADC |
		// VDD reference.
		DAC_CTRL_REFSEL_VDD |
		// Prescaler 2^4.
		4 << _DAC_CTRL_PRESC_SHIFT |
		// Refresh every 8 cycles.
		DAC_CTRL_REFRSEL_8CYCLES;
	DAC0->OPA0MUX =
		// Enable main output for channel 0.
		DAC_OPA0MUX_OUTMODE_MAIN;
#endif
	DAC0->OPA1MUX =
		// No resistor network
		DAC_OPA1MUX_RESSEL_RES0 |
		DAC_OPA1MUX_RESINMUX_DISABLE |
		// IN+ from DAC
		DAC_OPA1MUX_POSSEL_POSPAD |
		DAC_OPA1MUX_PPEN |
		// IN- from pin
		DAC_OPA1MUX_NEGSEL_NEGPAD |
		DAC_OPA1MUX_NPEN |
		// Enable alternate output to pin and ADC CH1
		DAC_OPA1MUX_OUTMODE_ALT |
		DAC_OPA1MUX_OUTPEN_OUT3 |
#if ADC_INPUT == ADC_INPUT_TIA
		// Enable ADC output
		DAC_OPA1MUX_OUTPEN_OUT4;
#else
		// Enable output to OPA2
		DAC_OPA1MUX_NEXTOUT;
	DAC0->OPA2MUX =
		// Enable IN+ PAD
		DAC_OPA2MUX_PPEN |
		// No resistor network
		DAC_OPA2MUX_RESSEL_RES1 |
		DAC_OPA2MUX_RESINMUX_OPA1INP |
		// IN+ from OPA1.
		DAC_OPA2MUX_POSSEL_POSPAD |
		// IN- from UG feedback.
		DAC_OPA2MUX_NEGSEL_OPATAP |
		// Enable main output
		DAC_OPA2MUX_OUTMODE |
		DAC_OPA2MUX_OUTPEN_OUT0;
#endif
}

void opamp_enable(uint16_t offset)
{
	// Enable DAC0 clock.
	BITBAND(&CMU->HFPERCLKEN0, _CMU_HFPERCLKEN0_DAC0_SHIFT) = 1;
#if ADC_INPUT == ADC_INPUT_AMP
	// Enable DAC output.
	BITBAND(&DAC0->CH0CTRL, _DAC_CH0CTRL_EN_SHIFT) = 1;
	BITBAND(&DAC0->CH1CTRL, _DAC_CH1CTRL_EN_SHIFT) = 1;
	// Enable OPA0-2
	DAC0->OPACTRL = DAC_OPACTRL_OPA1EN | DAC_OPACTRL_OPA2EN;
	// Convert offset for OPA2.
	DAC0->IFC = DAC_IFC_CH0;
	DAC0->CH0DATA = 2048;
	while(!(DAC0->IF & DAC_IF_CH0));
#else
	// Enable DAC output.
	BITBAND(&DAC0->CH1CTRL, _DAC_CH1CTRL_EN_SHIFT) = 1;
	// Enable OPA0-2
	DAC0->OPACTRL = DAC_OPACTRL_OPA1EN;
#endif
}

void opamp_disable()
{
#if !DISABLE_POWERSAVE
	// Disable OPA0-2
	DAC0->OPACTRL = 0;
	// Disable DAC output.
#if ADC_INPUT == ADC_INPUT_AMP
	BITBAND(&DAC0->CH0CTRL, _DAC_CH0CTRL_EN_SHIFT) = 0;
#endif
	BITBAND(&DAC0->CH1CTRL, _DAC_CH1CTRL_EN_SHIFT) = 0;
	// Disable DAC0 clock.
	BITBAND(&CMU->HFPERCLKEN0, _CMU_HFPERCLKEN0_DAC0_SHIFT) = 0;
#endif
}
