#pragma once

#include "../iir.hpp"

static const Math::Q31 _filt_display_coeffs[] {
	// Section 1
	0.5000000000000,
	0.0000000000000,
	-0.5000000000000,
	0.4036630420408,
	-0.0465663308148,
};
Math::IIR<Math::Q31, 1> _filt_display(_filt_display_coeffs, 1);
