#pragma once

void lftimer_init();
void lftimer_stop();
void lftimer_start(int period);
void lftimer_wait();

void timer_init();
void timer_start(int period);
void timer_enable();
void timer_disable();
