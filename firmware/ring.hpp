#pragma once

#include <stddef.h>

template<size_t SIZE>
class RingIndex
{
	static_assert((SIZE & (SIZE - 1)) == 0, "Size must be power-of-two.");
public:
	typedef int Type;

	static constexpr Type Size = SIZE;
	static constexpr Type Mask = SIZE - 1;

	RingIndex() {}
	explicit RingIndex(Type index)
		: m_index(index)
	{
	}

	operator Type() const { return get(); }
	Type get() const { return m_index; }

	RingIndex operator+(Type value) const { return static_cast<RingIndex>((m_index + value) & Mask); }
	RingIndex operator-(Type value) const { return static_cast<RingIndex>((m_index - value) & Mask); }
	RingIndex& operator+=(Type value) { m_index = operator+(value); return *this; }
	RingIndex& operator-=(Type value) { m_index = operator-(value); return *this; }
	RingIndex operator++() { RingIndex ret{*this}; operator+=(1); return ret; }
	RingIndex& operator++(int) { return operator+=(1); }
	RingIndex operator--() { RingIndex ret{*this}; operator-=(1); return ret; }
	RingIndex& operator--(int) { return operator-=(1); }

private:
	Type m_index = {};
};

template<typename T, size_t SIZE>
class Ring
{
public:
	typedef RingIndex<SIZE> IndexType;

	static constexpr typename IndexType::Type Size = IndexType::Size;

	IndexType index() const { return m_index; }
	Ring& add(const T& value) { m_array[m_index++] = value; }
	T& at(IndexType index) { return m_array[index]; }
	const T& at(IndexType index) const { return m_array[index]; }
	void copyTo(T* target)
	{
		int countToEnd = (SIZE - m_index.get());
		int countFromStart = m_index.get();
		memcpy(target, m_array + countFromStart, sizeof(T) * countToEnd);
		memcpy(target + countToEnd, m_array, sizeof(T) * countFromStart);
	}

	Ring& operator<<(const T& value) { return add(value); }
	T& operator[](IndexType index) { return at(index); }
	const T& operator[](IndexType index) const { return at(index); }

private:
	T m_array[IndexType::Size];
	IndexType m_index;
};
