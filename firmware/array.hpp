#pragma once

#include <stddef.h>

template<typename T, size_t SIZE>
class Array
{
public:
	typedef T Type;
	static constexpr size_t Size = SIZE;

	Array() {}
	Array(const T* elements)
		: m_elements(elements)
	{
	}
	Type& operator[](size_t index) { return m_elements[index]; };
	const Type& operator[](size_t index) const { return m_elements[index]; }

private:
	Type m_elements[Size] = {};
};
