#pragma once

void display_init();
void display_draw_zoom(int level);
void display_draw_text(int x, int y, const char* text);
void display_set_pixel(int line, int pixel, int value);
void display_shift_lines(int activeLine);
void display_shift_lines_edge(int activeLine);
void display_draw_bpm(int value);
void display_draw_notch(int oldPixel, int newPixel);
void display_render();
