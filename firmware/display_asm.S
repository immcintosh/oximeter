
    #define THUMB .thumb
    #define CODESECT .section .text
    #define EXPORT .global
    #define PROC :
    #define LABEL :
    #define ENDP
    #define END

    .syntax unified

	CODESECT
	THUMB

	EXPORT display_shift_pixel

# R0 = line address
# R1 = initial pixel
display_shift_pixel PROC
	# Shift the initial pixel into the carry flag.
	LSRS	r1, r1, #1
	# Pixels 0-31
	LDR		r1, [r0, #0]
	RBIT	r1, r1
	RRXS	r1, r1
	RBIT	r1, r1
	STR		r1, [r0, #0]
	# Pixels 32-63
	LDR		r1, [r0, #4]
	RBIT	r1, r1
	RRXS	r1, r1
	RBIT	r1, r1
	STR		r1, [r0, #4]
	# Return from procedure
	BX		lr
	ENDP
