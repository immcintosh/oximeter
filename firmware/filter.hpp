#pragma once

#include "fixed.hpp"

namespace Math
{
template<typename T>
class HighPass
{
public:
	HighPass(T alpha)
		: m_alpha(alpha)
		, m_last(0)
		, m_value(0)
	{
	}
	T filter(T value)
	{
		m_value = m_alpha * (m_value + value - m_last);
		m_last = value;
		return m_value;
	}

private:
	T m_alpha;
	T m_last;
	T m_value;
};

template<typename T>
class LowPass
{
public:
	LowPass(Q15 alpha)
		: m_alpha(alpha)
		, m_value(0)
	{
	}
	T filter(T value)
	{
		m_value = m_value + m_alpha * (value - m_value);
		return m_value;
	}

private:
	T m_alpha;
	T m_value;
};
}
