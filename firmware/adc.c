#include "main.h"

void adc_init()
{
	CMU->HFPERCLKEN0 |=
		// Enable ADC clock
		CMU_HFPERCLKEN0_ADC0 |
		// Enable GPIO clock
		CMU_HFPERCLKEN0_GPIO;
	ADC0->CTRL =
		// Enable decoupling capacitor
		ADC_CTRL_LPFMODE_DECAP |
		// Set warmup timebase above 1us
		(SystemPeripheralClock / 1000000 + 1) << 16 |
		// 16x oversampling
		ADC_CTRL_OVSRSEL_X16;
	ADC0->SINGLECTRL =
		// Always left adjust samples so math always works correctly
		ADC_SINGLECTRL_ADJ_LEFT |
		// Enable oversampling
		ADC_SINGLECTRL_RES_OVS |
		// OPA1 input on CH1
#if ADC_INPUT == ADC_INPUT_TIA
		ADC_SINGLECTRL_INPUTSEL_CH1 |
#else
		ADC_SINGLECTRL_INPUTSEL_CH5 |
#endif
		// Buffered VDD reference
		ADC_SINGLECTRL_REF_VDD |
		// Enable PRS trigger
		ADC_SINGLECTRL_PRSEN |
		// Select PRS channel 0
		ADC_SINGLECTRL_PRSSEL_PRSCH0;
	// Turn LED off
	GPIO->P[0].DOUTSET = 1 << 8 | 1 << 9;
	GPIO->P[0].MODEH |=
		// Push-Pull A8
		GPIO_P_MODEH_MODE8_PUSHPULL |
		// Push-Pull A9
		GPIO_P_MODEH_MODE9_PUSHPULL;
	// Clear interrupts.
	ADC0->IFC = ADC_IFC_SINGLE;
	// Enable single conversion complete interrupt
	ADC0->IEN = ADC_IEN_SINGLE;
}

void adc_enable()
{
	// Enable ADC clock
	BITBAND(&CMU->HFPERCLKEN0, 9) = 1;
	ADC0->SINGLECTRL =
		// Always left adjust samples so math always works correctly
		ADC_SINGLECTRL_ADJ_LEFT |
		// Enable oversampling
		ADC_SINGLECTRL_RES_OVS |
		// ADC input on selection
		ADC_INPUT |
		// Buffered VDD reference
		ADC_SINGLECTRL_REF_VDD |
		// Enable PRS trigger
		ADC_SINGLECTRL_PRSEN |
		// Select PRS channel 0
		ADC_SINGLECTRL_PRSSEL_PRSCH0;
	// Turn LED on
	GPIO->P[0].DOUTCLR = 1 << 8 | 1 << 9;
}

void adc_disable()
{
#if !DISABLE_POWERSAVE
	// Disable ADC clock
	BITBAND(&CMU->HFPERCLKEN0, 9) = 0;
	// Turn LED off
	GPIO->P[0].DOUTSET = 1 << 8 | 1 << 9;
#endif
}

void adc_sample()
{
	// Start sample.
	ADC0->CMD = ADC_CMD_SINGLESTART;
	// Wait for single conversion complete.
	do {
		__WFE();
	} while(!(ADC0->IF & ADC_IFS_SINGLE));
	// Clear ADC interrupt
	ADC0->IFC = ADC_IFC_SINGLE;
	NVIC_ClearPendingIRQ(ADC0_IRQn);
}

uint16_t adc_get_sample()
{
	return ADC0->SINGLEDATA;
}
